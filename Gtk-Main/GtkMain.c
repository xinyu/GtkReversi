/*************************************************
* Description	: Gtk Based Reversi Board Template
* Author			: XinYu Lin
* E-mail			: nxforce@yahoo.com
* Date			: 2008.09.09
*************************************************/

#include <gtk/gtk.h>

#include "interface.h"
#include "definition.h"


int main (int argc, char *argv[])
{
	/*** Create ChatData pointer ***/
	GtkData* gtkData;
	
	/*** allocate ChatData struct ***/
	gtkData = g_slice_new (GtkData);
	
	/***  Initialize GTK ***/
	gtk_set_locale ();	
	gtk_init (&argc, &argv);	
	
	/*** initialize GUI ***/
	initGUI(gtkData);
	
	/*** main() loop ***/
	gtk_main();
	
	/*** free chatData ***/
   g_slice_free (GtkData, gtkData);
	
	return 0;
}




