/********************************
* signal callback definition
*********************************/

#ifndef __CALLBACK_H__
#define __CALLBACK_H__

#include <stdio.h>
#include <gtk/gtk.h>

#include "definition.h"

/*======= signal handler for window ==============*/
void on_window_destroy (GtkObject *object, GtkData* gtkData);


/*======= signal handler for buttons ==============*/
void on_button_clicked (GtkButton* button, GtkData* gtkData);
void on_clean_button_clicked (GtkButton* button, GtkData* gtkData);

int on_configure_event(GtkWidget *drawArea ,GdkEventConfigure *event, GtkData* gtkData);
int on_expose_event(GtkWidget *widget , GdkEventExpose *event , GtkData* gtkData);
int on_button_press_event(GtkWidget *drawArea , GdkEventButton *event, GtkData* gtkData);

void on_about_menu_item_activate (GtkMenuItem *menuitem, GtkData* gtkData);
void on_start_button_clicked (GtkMenuItem *menuitem, GtkData* gtkData);
void on_restart_button_clicked (GtkMenuItem *menuitem, GtkData* gtkData);

void show_pixmap(GtkData* gtkData);

#endif
