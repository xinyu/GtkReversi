/***********************************************
* interface definition for Gtk+ Reversi Board
************************************************/

#ifndef __INTERFACE_H__
#define __INTERFACE_H__

#include <gtk/gtk.h>



#include "callback.h"
#include "definition.h"

void initGUI(GtkData* gtkData);
GdkPixbuf* create_pixbuf (const gchar *filename);

#endif
