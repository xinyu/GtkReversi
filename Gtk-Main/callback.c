/******************************************
* Signal callback for Gtk+ Reversi Board
*******************************************/

#include "callback.h"

/*======= signal handler for window ==============*/
void on_window_destroy (GtkObject *object, GtkData* gtkData)
{
	gtk_main_quit();
}

/*======= signal handler for button ==============*/

int on_configure_event(GtkWidget *drawArea ,GdkEventConfigure *event, GtkData* gtkData)
{
	int i;
	GError *error = NULL;
	GdkPixbuf *image = NULL;
   
	/*** unreference pixmap ***/
	if(gtkData->pixmap) g_object_unref(gtkData->pixmap);
   
   /*** create a new pixmap ***/   
	gtkData->pixmap = gdk_pixmap_new(	drawArea->window ,
													drawArea->allocation.width ,
													drawArea->allocation.height ,
													-1										
												);
												
	/*** load black piece image ***/
	image = gdk_pixbuf_new_from_file("../pixmaps/bgcolor.png",&error);
	
	if (error) {
		g_warning (G_STRLOC ": load black image error: %s\n", error->message);
		g_error_free (error);
		error = NULL;
	}
	
	/*** convert GdkPixbuf to GdkPixmap ***/
	gdk_pixbuf_render_pixmap_and_mask_for_colormap (	image ,
																		gdk_colormap_get_system () ,
																		&gtkData->piece[2] ,
																		&gtkData->pieceMask[2] ,
																		127
																	);											
												
   /*** draw background image rectangle ***/
   gdk_draw_drawable(	gtkData->pixmap ,
								drawArea->style->fg_gc[GTK_WIDGET_STATE(drawArea)], 
								gtkData->piece[2] , 
								0 , 0 , 
								0 , 0 , 
								drawArea->allocation.width , drawArea->allocation.height
							);               
	
	/*** load vertical label image ***/
	image = gdk_pixbuf_new_from_file("../pixmaps/vlabel.png",&error);
	
	if (error) {
		g_warning (G_STRLOC ": load black image error: %s\n", error->message);
		g_error_free (error);
		error = NULL;
	}
	
	/*** convert GdkPixbuf to GdkPixmap ***/
	gdk_pixbuf_render_pixmap_and_mask_for_colormap (	image ,
																		gdk_colormap_get_system () ,
																		&gtkData->piece[3] ,
																		&gtkData->pieceMask[3] ,
																		127
																	);
	
	/*** draw vertical start label image ***/
   gdk_draw_drawable(	gtkData->pixmap ,
								drawArea->style->fg_gc[GTK_WIDGET_STATE(drawArea)], 
								gtkData->piece[3] , 
								0 , 0 , 
								0 			, YOFFSET +YBORDER , 
								XOFFSET	, (BOARDHEIGHT)
							); 
	
	/*** draw vertical end label image ***/
   gdk_draw_drawable(	gtkData->pixmap ,
								drawArea->style->fg_gc[GTK_WIDGET_STATE(drawArea)], 
								gtkData->piece[3] , 
								0 , 0 , 
								XOFFSET +2*(XBORDER)	+(BOARDHEIGHT)	, YOFFSET +YBORDER , 
								XOFFSET 										, (BOARDHEIGHT)
							); 
	
	
	/*** load horizontal label image ***/
	image = gdk_pixbuf_new_from_file("../pixmaps/hlabel.png",&error);
	
	if (error) {
		g_warning (G_STRLOC ": load black image error: %s\n", error->message);
		g_error_free (error);
		error = NULL;
	}	
	
	/*** convert GdkPixbuf to GdkPixmap ***/
	gdk_pixbuf_render_pixmap_and_mask_for_colormap (	image ,
																		gdk_colormap_get_system () ,
																		&gtkData->piece[4] ,
																		&gtkData->pieceMask[4] ,
																		127
																	);
		
	/*** draw horizontal start label image ***/
   gdk_draw_drawable(	gtkData->pixmap ,
								drawArea->style->fg_gc[GTK_WIDGET_STATE(drawArea)], 
								gtkData->piece[4] , 
								0 , 0 , 
								XOFFSET+XBORDER 	, 0 , 
								(BOARDWIDTH) 		, YOFFSET
							);
							
	/*** draw horizontal end label image ***/
   gdk_draw_drawable(	gtkData->pixmap ,
								drawArea->style->fg_gc[GTK_WIDGET_STATE(drawArea)], 
								gtkData->piece[4] , 
								0 , 0 , 
								XOFFSET+XBORDER	, YOFFSET+2*(YBORDER)+(BOARDHEIGHT), 
								(BOARDWIDTH) 		, YOFFSET
							);
	
	/*** draw outer border ***/
	gdk_draw_rectangle(	gtkData->pixmap, 
								drawArea->style->black_gc ,
								FALSE , 
								0 													, 0 ,
								(BOARDWIDTH) +2*(XBORDER) +2*XOFFSET -1	, (BOARDHEIGHT) +2*(YBORDER) +2*YOFFSET -1
							);
							            
	gdk_draw_rectangle(	gtkData->pixmap, 
								drawArea->style->black_gc ,
								FALSE , 
								XOFFSET , YOFFSET ,
								(BOARDWIDTH)+2*(XBORDER) -1, (BOARDHEIGHT)+2*(YBORDER) -1
							);
							
	gdk_draw_rectangle(	gtkData->pixmap, 
								drawArea->style->black_gc ,
								FALSE , 
								XOFFSET -1 					, YOFFSET -1,
								(BOARDWIDTH)+2*(XBORDER) +1, (BOARDHEIGHT)+2*(YBORDER) +1
							);
							
	/*** draw inner border ***/            
	gdk_draw_rectangle(	gtkData->pixmap, 
								drawArea->style->black_gc ,
								FALSE , 
								XOFFSET+XBORDER , YOFFSET+YBORDER ,
								(BOARDWIDTH) -1, (BOARDHEIGHT) -1
							);        

	/*** draw grid ***/
	for (i = 1; i < XMAX; i++) {
	
			/*** vertical line ***/
			gdk_draw_line (	gtkData->pixmap, 
									drawArea->style->black_gc, 
									XOFFSET +XBORDER + i * BOARDWIDTH  / XMAX - 1,	YOFFSET +YBORDER, 
									XOFFSET +XBORDER + i * BOARDWIDTH  / XMAX - 1,	YOFFSET +YBORDER+BOARDHEIGHT-1
								);
							
			/*** horizontal line ***/					
    		gdk_draw_line (	gtkData->pixmap, 
    								drawArea->style->black_gc, 
    								XOFFSET +XBORDER,		 					YOFFSET +YBORDER + i * BOARDHEIGHT / YMAX - 1, 
    								XOFFSET +XBORDER +BOARDWIDTH -1 ,	YOFFSET +YBORDER + i * BOARDHEIGHT / YMAX - 1
    							);
	}

	

	/*** load black piece image ***/
	image = gdk_pixbuf_new_from_file("../pixmaps/black.png",&error);
	
	if (error) {
		g_warning (G_STRLOC ": load black image error: %s\n", error->message);
		g_error_free (error);
		error = NULL;
	}
	
	/*** convert GdkPixbuf to GdkPixmap ***/
	gdk_pixbuf_render_pixmap_and_mask_for_colormap (	image ,
																		gdk_colormap_get_system () ,
																		&gtkData->piece[0] ,
																		&gtkData->pieceMask[0] ,
																		127
																	);
						  
	/*** load white piece image ***/
	image = gdk_pixbuf_new_from_file("../pixmaps/white.png",&error);
	
	if (error) {
		g_warning (G_STRLOC ": load black image error: %s\n", error->message);
		g_error_free (error);
		error = NULL;
	}
	
	/*** convert GdkPixbuf to GdkPixmap ***/
	gdk_pixbuf_render_pixmap_and_mask_for_colormap (	image ,
																		gdk_colormap_get_system () ,
																		&gtkData->piece[1] ,
																		&gtkData->pieceMask[1] ,
																		127
																	);
	
  	gdk_pixbuf_unref (image);	
      
	return 0;
}

int on_expose_event(GtkWidget *drawArea , GdkEventExpose *event , GtkData* gtkData)
{
	show_pixmap(gtkData); 
	return 0;
}

int on_button_press_event(GtkWidget *drawArea , GdkEventButton *event, GtkData* gtkData)
{
	int x,y;
	
	if(	((event->x -XOFFSET -(XBORDER)) < 0 ) ||
			((event->y -YOFFSET -(YBORDER)) < 0 ) ||
			((event->x -XOFFSET -(XBORDER)) > BOARDWIDTH  ) ||
			((event->y -YOFFSET -(YBORDER)) > BOARDHEIGHT ) 	
		)	return 0;
	
	x=(event->x-XOFFSET -(XBORDER))/(BOARDWIDTH)*XMAX;
	y=(event->y-YOFFSET -(YBORDER))/(BOARDHEIGHT)*YMAX;
	
	printf("x=%d y=%d\n" ,x ,y) ;
	
	switch(event->button){
		case 1:
			gdk_draw_drawable(	gtkData->pixmap ,
										drawArea->style->fg_gc[GTK_WIDGET_STATE(drawArea)], 
										gtkData->piece[0] , 
										0 , 0 , 
										XOFFSET +XBORDER +GRIDWIDTH +x*TILEWIDTH	, YOFFSET +(YBORDER) +GRIDWIDTH +y*TILEHEIGHT , 
										TILEWIDTH-2*GRIDWIDTH							, TILEHEIGHT-2*GRIDWIDTH
									);
			
			break;
			
		case 2:
			gdk_draw_drawable(	gtkData->pixmap ,
										drawArea->style->fg_gc[GTK_WIDGET_STATE(drawArea)], 
										gtkData->piece[2] , 
										0 , 0 , 
										XOFFSET +XBORDER +GRIDWIDTH +x*TILEWIDTH	, YOFFSET +(YBORDER) +GRIDWIDTH +y*TILEHEIGHT , 
										TILEWIDTH-2*GRIDWIDTH 							, TILEHEIGHT-2*GRIDWIDTH
									);
			break;
				
		case 3:
			gdk_draw_drawable(	gtkData->pixmap ,
										drawArea->style->fg_gc[GTK_WIDGET_STATE(drawArea)], 
										gtkData->piece[1] , 
										0 , 0 , 
										XOFFSET +XBORDER +GRIDWIDTH +x*TILEWIDTH	, YOFFSET +(YBORDER) +GRIDWIDTH +y*TILEHEIGHT , 
										TILEWIDTH-2*GRIDWIDTH 							, TILEHEIGHT-2*GRIDWIDTH
									);
			break;
	}
	
	show_pixmap(gtkData);
	     
	return 1;    
}

void show_pixmap(GtkData* gtkData)
{
	gdk_draw_drawable(	gtkData->drawArea->window ,
								gtkData->drawArea->style->fg_gc[GTK_WIDGET_STATE(gtkData->drawArea)] ,
								gtkData->pixmap , 
								0 , 0 , 
								0 , 0 , 
								gtkData->drawArea->allocation.width ,
								gtkData->drawArea->allocation.height
							); 	
}

void on_clean_button_clicked (GtkButton* button, GtkData* gtkData)
{
	int x,y;
	
	for(x=0;x<XMAX;x++){
		for(y=0;y<YMAX;y++){
			gdk_draw_drawable(	gtkData->pixmap,
										gtkData->drawArea->style->fg_gc[GTK_WIDGET_STATE(gtkData->drawArea)], 
										gtkData->piece[2] , 
										0 , 0 ,
										XOFFSET+ GRIDWIDTH+x*TILEWIDTH , YOFFSET+ GRIDWIDTH+y*TILEHEIGHT , 
										TILEWIDTH-2*GRIDWIDTH , TILEHEIGHT-2*GRIDWIDTH
									);				
		}
	}
	
	show_pixmap(gtkData);
}

/*====== signal handler for menubar =============*/
void on_about_menu_item_activate (GtkMenuItem *menuitem, GtkData* gtkData)
{
	static const gchar * const authors[] = {"Xin-Yu Lin <nxforce@yahoo.com>", NULL};

	static const gchar copyright[] = "Copyright \xc2\xa9 2008 Xin-Yu Lin";

	static const gchar comments[] = "GTK+2.0 Programming";

	gtk_show_about_dialog (GTK_WINDOW (gtkData->window),
			       "authors", authors,
			       "comments", comments,
			       "copyright", copyright,
			       "version", "0.1",
			       "website", "http://nxforce.blogspot.com",
			       "program-name", "GTK+ Reversi Board Template",
			       "logo-icon-name", GTK_STOCK_EDIT,
			       NULL); 
}

void on_start_button_clicked (GtkMenuItem *menuitem, GtkData* gtkData)
{

}

void on_restart_button_clicked (GtkMenuItem *menuitem, GtkData* gtkData)
{

}

