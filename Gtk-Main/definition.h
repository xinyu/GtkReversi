/****************************************
* struct definition for Reversi Board
*****************************************/

#ifndef __DEFINITION_H__
#define __DEFINITION_H__

#define XMAX	19
#define YMAX	19

#define TILEWIDTH	40
#define TILEHEIGHT 40

#define XOFFSET 30
#define YOFFSET 30

#define XBORDER TILEWIDTH/2
#define YBORDER TILEHEIGHT/2

#define BOARDWIDTH XMAX*TILEWIDTH
#define BOARDHEIGHT YMAX*TILEHEIGHT



#define GRIDWIDTH 1


 
typedef struct {
	
	GtkWidget *window;
	GtkWidget* vbox;	
	
	GtkWidget* statusbar;
	GtkWidget *drawArea;
	
	GtkWidget *menubar;
  
	GtkWidget *menuitem_game;
	GtkWidget *menu_game;
	GtkWidget *start_m;
	GtkWidget *restart_m;
	GtkWidget *separatormenuitem_m;
	GtkWidget *quit_m;
	
	GtkWidget *menuitem_help;
	GtkWidget *menu_help;
	GtkWidget *about_m;

	GdkPixmap *pixmap;
	
	GdkPixmap *piece[5];
	GdkPixmap *pieceMask[5];
	
}GtkData;

#endif
