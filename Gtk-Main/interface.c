/***********************************************
* interface for Gtk+ Reversi Board
************************************************/

#include "interface.h"

void initGUI(GtkData* gtkData)
{	
	
	/*** Create the new window ***/
	gtkData->window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title (GTK_WINDOW (gtkData->window), "Gtk Template");
	gtk_window_set_default_size (GTK_WINDOW (gtkData->window), BOARDWIDTH+2*XOFFSET+2*XBORDER, BOARDHEIGHT+2*YOFFSET+2*YBORDER);
	gtk_container_set_border_width(GTK_CONTAINER(gtkData->window) , 0);
	gtk_window_set_resizable(GTK_WINDOW(gtkData->window) , FALSE);
	
	/*
	gtkData->window_icon_pixbuf = create_pixbuf ("../pixmaps/GClient.gif");
  	gtk_window_set_icon (GTK_WINDOW (gtkData->window), gtkData->window_icon_pixbuf);
	*/
	
	gtkData->vbox = gtk_vbox_new(FALSE , 0);
	gtk_container_add(GTK_CONTAINER(gtkData->window) , gtkData->vbox);
	gtk_widget_show(gtkData->vbox);
	
	/*** menu bar *********/
	gtkData->menubar = gtk_menu_bar_new ();
	gtk_widget_show (gtkData->menubar);
	gtk_box_pack_start (GTK_BOX (gtkData->vbox), gtkData->menubar, FALSE, FALSE, 0);

	/*** menu item - client ***/
  	gtkData->menuitem_game = gtk_menu_item_new_with_label ("Game");
  	gtk_widget_show (gtkData->menuitem_game);
  	gtk_container_add (GTK_CONTAINER (gtkData->menubar), gtkData->menuitem_game);

	/*** menu - client ***/
	gtkData->menu_game = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (gtkData->menuitem_game), gtkData->menu_game);
	
	/*** label - connect ***/
	gtkData->start_m = gtk_menu_item_new_with_label ("Start");
	gtk_widget_show (gtkData->start_m);
	gtk_container_add (GTK_CONTAINER (gtkData->menu_game), gtkData->start_m);

	/*** label - disconnect ***/
	gtkData->restart_m = gtk_menu_item_new_with_label ("Restart");
	gtk_widget_show (gtkData->restart_m);
	gtk_container_add (GTK_CONTAINER (gtkData->menu_game), gtkData->restart_m);
	
	/*** separator ***/
	gtkData->separatormenuitem_m = gtk_separator_menu_item_new ();
	gtk_widget_show (gtkData->separatormenuitem_m);
	gtk_container_add (GTK_CONTAINER (gtkData->menu_game), gtkData->separatormenuitem_m);
	gtk_widget_set_sensitive (gtkData->separatormenuitem_m, FALSE);

	/*** label - quit ***/
	gtkData->quit_m = gtk_menu_item_new_with_label ("Quit");
	gtk_widget_show (gtkData->quit_m);
	gtk_container_add (GTK_CONTAINER (gtkData->menu_game), gtkData->quit_m);
	
	gtkData->drawArea = gtk_drawing_area_new ();
	gtk_box_pack_start(GTK_BOX(gtkData->vbox) ,gtkData->drawArea, TRUE , TRUE , 0);
	gtk_widget_set_size_request (GTK_WIDGET (gtkData->drawArea), BOARDWIDTH+2*XOFFSET+2*XBORDER, BOARDHEIGHT+2*YOFFSET+2*YBORDER);	
	gtk_widget_show(gtkData->drawArea);
	
	/*** menu item - Help ***/
	gtkData->menuitem_help = gtk_menu_item_new_with_label ("Help");
	gtk_widget_show (gtkData->menuitem_help);
	gtk_container_add (GTK_CONTAINER (gtkData->menubar), gtkData->menuitem_help);
	
	/*** menu - help ***/
	gtkData->menu_help = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (gtkData->menuitem_help), gtkData->menu_help);

	/*** label - About ***/
	gtkData->about_m = gtk_menu_item_new_with_label ("About");
	gtk_widget_show (gtkData->about_m);
	gtk_container_add (GTK_CONTAINER (gtkData->menu_help), gtkData->about_m);
	
	/*** status bar ***/
	gtkData->statusbar = gtk_statusbar_new ();
	gtk_widget_show (gtkData->statusbar);
	gtk_box_pack_start (GTK_BOX (gtkData->vbox), gtkData->statusbar, FALSE, TRUE, 0);
	//gtk_widget_set_size_request (gtkData->statusbar, 20, -1);
		
	
	/*** singals ***/
	g_signal_connect (gtkData->window, "destroy", G_CALLBACK (on_window_destroy), gtkData);
	g_signal_connect(G_OBJECT(gtkData->drawArea) , "button_press_event" , G_CALLBACK(on_button_press_event) , gtkData);
	
	g_signal_connect(G_OBJECT(gtkData->drawArea) , "configure_event" , G_CALLBACK(on_configure_event) , gtkData);
	g_signal_connect(G_OBJECT(gtkData->drawArea) , "expose_event" , G_CALLBACK(on_expose_event) , gtkData);
	gtk_widget_set_events (gtkData->drawArea,  GDK_EXPOSURE_MASK | GDK_BUTTON_PRESS_MASK);

	g_signal_connect (gtkData->start_m, "activate", G_CALLBACK (on_start_button_clicked), gtkData);
	g_signal_connect (gtkData->restart_m, "activate", G_CALLBACK (on_restart_button_clicked), gtkData);
	g_signal_connect (gtkData->quit_m, "activate", G_CALLBACK (on_window_destroy), gtkData);
	g_signal_connect (gtkData->about_m, "activate", G_CALLBACK (on_about_menu_item_activate), gtkData);

	/*** show window ***/
	gtk_widget_show (gtkData->window);
}

GdkPixbuf *create_pixbuf(const gchar * filename)
{
   GdkPixbuf *pixbuf;
   GError *error = NULL;
   pixbuf = gdk_pixbuf_new_from_file(filename, &error);
   if(!pixbuf) {
      fprintf(stderr, "%s\n", error->message);
      g_error_free(error);
   }
   return pixbuf;
}
